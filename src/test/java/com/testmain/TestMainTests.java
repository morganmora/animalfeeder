package com.testmain;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestMainTests {
	
	private final ByteArrayOutputStream out = new ByteArrayOutputStream();
	private final ByteArrayOutputStream err = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(out));
	    System.setErr(new PrintStream(err));
	}

	@Test
	public void testFeedAnimalMonkey() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method feedAnimals = TestMain.class.getDeclaredMethod("feedAnimals", String.class, int.class);
		feedAnimals.setAccessible(true);
		feedAnimals.invoke(null, "morgan", 50);
		String expected = "The animal Cheeta has been fed by morgan\r\n";
		expected += "The animal Mufasa has been fed by morgan\r\n";
		Assert.assertEquals(expected, out.toString());
	}
	
	@After
	public void restoreStreams() {
	    System.setOut(System.out);
	    System.setErr(System.err);
	}

}
