package com.testmain;

import com.testmain.model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
public class TestMain {

	private static final String REST_URL = "http://jsonplaceholder.typicode.com/users";
	private static List<Animal> animals = new ArrayList<Animal>();

	public static void main(String[] args) {
		SpringApplication.run(TestMain.class, args).close();
	}

	/**
	 * synchronous client-side HTTP access.
	 * 
	 * @param builder
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	/**
	 * Command line runner for this app.
	 * 
	 * @param restTemplate
	 * @param appArgs
	 * @return
	 * @throws Exception
	 */
	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
	public CommandLineRunner run(RestTemplate restTemplate, final ApplicationArguments appArgs) throws Exception {
		return args -> {

			// Initialize food per users
			int foodPerUser = 0;

			// Init animals
			animals.add(new Monkey("Cheeta"));
			animals.add(new Lion("Mufasa"));
			animals.add(new Lion("Nala"));
			animals.add(new Lion("Simba"));
			animals.add(new Monkey("Tuco"));

			// sort the animals.
			animals.sort(Comparator.comparing(Animal::getName));

			// Get users from: http://jsonplaceholder.typicode.com/users
			List<String> users = getUsers(restTemplate.getForObject(REST_URL, String.class));

			for (String argument : appArgs.getNonOptionArgs()) {
				foodPerUser = Integer.valueOf(argument);
			}

			for (String user : users) {
				feedAnimals(user, foodPerUser);
			}
		};

	}

	/**
	 * Animals feeder method.
	 * 
	 * @param user
	 * @param foodPerUser
	 */
	private static void feedAnimals(String user, int foodPerUser) {
		int remainigFood = foodPerUser;
		for (Iterator<Animal> iterator = animals.iterator(); iterator.hasNext();) {
			Animal animal = iterator.next();
			if (animal.getFeeded()) {
				continue;
			}
			if (animal.getFoodUnits() > remainigFood) {
				break;
			}
			animal.setFeeded(true);
			remainigFood -= animal.getFoodUnits();
			System.out.println("The animal " + animal.getName() + " has been fed by " + user);
		}
	}

	/**
	 * Deserialize the names of users from Json array.
	 * 
	 * @param string
	 * @return
	 * @throws Exception
	 */
	private List<String> getUsers(String string) throws Exception {
		List<String> list = new ArrayList<>();
		JSONArray jsonArray = new JSONArray(string);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject user = jsonArray.getJSONObject(i);
			list.add(user.getString("name"));
		}
		return list;
	}

}
