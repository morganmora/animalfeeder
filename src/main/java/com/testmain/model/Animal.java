package com.testmain.model;

/**
 * Generic class that represent Animal
 * @author Martinez
 *
 */
public class Animal {

	private String name;
	private final int foodUnits;
	private Boolean feeded;

	/**
	 * @param name
	 * @param foodUnits
	 * @param feeded
	 */
	public Animal(String name, int foodUnits, Boolean feeded) {
		super();
		this.name = name;
		this.foodUnits = foodUnits;
		this.feeded = feeded;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public final void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the feeded
	 */
	public final Boolean getFeeded() {
		return feeded;
	}

	/**
	 * @param feeded
	 *            the feeded to set
	 */
	public final void setFeeded(Boolean feeded) {
		this.feeded = feeded;
	}

	/**
	 * @return the foodUnits
	 */
	public final int getFoodUnits() {
		return foodUnits;
	}

}
