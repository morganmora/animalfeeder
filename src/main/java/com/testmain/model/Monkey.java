/**
 * Mnokey class, inherit from animal
 */
package com.testmain.model;

/**
 * Monkey class
 * @author Martinez
 *
 */
public class Monkey extends Animal {

	/**
	 * Food units needed by Monkeys
	 */
	public static final int FOOD_UNITS = 10;

	/**
	 * @param name
	 */
	public Monkey(String name) {
		super(name, FOOD_UNITS, false);
	}
}