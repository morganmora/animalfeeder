/**
 * Lion class, inherit from animal
 */
package com.testmain.model;

/**
 * Lion class
 * @author Martinez
 *
 */
public class Lion extends Animal {
	
	/**
	 *Food units needed  to Lions 
	 */
	public static final int FOOD_UNITS = 30;
	
	/**
	 * @param name
	 */
	public Lion(String name) {
		super(name, FOOD_UNITS , false);
	}

}
